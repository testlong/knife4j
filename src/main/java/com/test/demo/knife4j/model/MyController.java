package com.test.demo.knife4j.model;

import com.test.demo.knife4j.controller.A;
import com.test.demo.knife4j.controller.MyRequest;
import com.test.demo.knife4j.controller.MyResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author laigl
 * @ClassName
 * @Description
 * @Date 2020/02/28 16:52
 */
@Api(tags = "目录名称", consumes = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping("web")
public class MyController {

    @PostMapping("test2")
    @ApiOperation(value = "接口标题", notes = "接口描述")
    public MyResponse test2(@RequestBody MyRequest req, BindingResult bindingResult) {
        return new MyResponse(new A());
    }

}
