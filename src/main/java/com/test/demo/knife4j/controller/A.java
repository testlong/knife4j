package com.test.demo.knife4j.controller;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author laigl
 * @ClassName
 * @Description
 * @Date 2020/02/28 17:18
 */
public class A {

    @ApiModelProperty(value = "aaaaaa")
    private String a;

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }
}
