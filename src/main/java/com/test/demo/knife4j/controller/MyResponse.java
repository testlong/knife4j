package com.test.demo.knife4j.controller;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author laigl
 * @ClassName
 * @Description
 * @Date 2020/02/28 16:50
 */
public class MyResponse<T> {

    public MyResponse(T t) {
        this.data = t;
    }

    /**
     * 业务消息
     */
    @ApiModelProperty(value = "失败信息")
    private String message;

    /**
     * 业务数据
     */
    @ApiModelProperty(value = "返回数据")
    private T data;

    /**
     * 业务状态码
     */
    @ApiModelProperty(value = "错误码")
    private String code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
