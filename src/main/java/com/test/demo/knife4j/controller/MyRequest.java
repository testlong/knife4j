package com.test.demo.knife4j.controller;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author laigl
 * @ClassName
 * @Description
 * @Date 2020/02/28 16:47
 */
public class MyRequest {

    @ApiModelProperty(value = "字符串参数",
            allowableValues = "0否,1是",
            required = true,
            example = "example")
    private String string;

    private Integer integer;

    private Object object;

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public Integer getInteger() {
        return integer;
    }

    public void setInteger(Integer integer) {
        this.integer = integer;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
